#include "StepperMotor.h"
#include "Button.h"

#define STEPS_TOTAL  2048
#define STEPS_UP     1

#define IN1          10
#define IN2          11
#define IN3          12
#define IN4          13

#define SPEED_BASE    5
#define BTTN_PIN      3

StepperMotor stepperMotor(STEPS_TOTAL, IN1, IN3, IN2, IN4);
Button button(BTTN_PIN);

void setup()
{
    stepperMotor.set_speed(SPEED_BASE);
}

void loop()
{
    if (button.pushed()) {
        while (!button.pushed()) {
            stepperMotor.move_steps(STEPS_UP);
        }
        stepperMotor.stop_motor();
    }
}