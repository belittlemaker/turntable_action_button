#ifndef Button_h
    #define Button_h
    
    #if defined(ARDUINO) && ARDUINO >= 100
        #include <Arduino.h>
        #else
        #include <WProgram.h>
    #endif
    
    class Button
    {      
        public:        
            Button(int pin);
            bool pushed();
        private:
            int pin; 
            int state;
            int read_state();
    };

#endif // Button_h