#include "Button.h"


Button::Button(int pin)
{
    this->pin = pin;
    this->state = LOW;
}

bool Button::pushed()
{
    int state_new = this->read_state();

    bool pushed = state_new == HIGH && this->state == LOW;
    
    this->state = state_new;

    return pushed;
}

int Button::read_state()
{
    return digitalRead(this->pin);
}