#ifndef StepperMotor_h
    #define StepperMotor_h
    
    #if defined(ARDUINO) && ARDUINO >= 100
        #include <Arduino.h>
        #else
        #include <WProgram.h>
    #endif
    #include <Stepper.h>
    
    class StepperMotor
    {      
        public:        
            StepperMotor(int steps, 
                         int in1,
                         int in2,
                         int in3,
                         int in4);
            ~StepperMotor();
            void move_degrees(long degrees_);
            void move_steps(long steps);
            void set_speed(long rpms);
            void stop_motor();
        private:
            int in1;
            int in2;
            int in3;
            int in4;
            int steps;
            Stepper* stepper;
            int get_steps_by_degrees(long degrees_);
    };

#endif // StepperMotor_h