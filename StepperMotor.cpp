#include "StepperMotor.h"


StepperMotor::StepperMotor(int steps, 
                           int in1,
                           int in2,
                           int in3,
                           int in4)
{
    this->in1 = in1;
    this->in2 = in2;
    this->in3 = in3;
    this->in4 = in4;
    this->steps = steps;
    this->stepper = new Stepper(steps, in1, in2, in3, in4);
}
StepperMotor::~StepperMotor()
{
    delete this->stepper;
}

void StepperMotor::set_speed(long rpms)
{
    this->stepper->setSpeed(rpms);
}

void StepperMotor::stop_motor()
{
    digitalWrite(this->in1, LOW);
    digitalWrite(this->in2, LOW);
    digitalWrite(this->in3, LOW);
    digitalWrite(this->in4, LOW);
}

void StepperMotor::move_degrees(long degrees_)
{
    int steps = this->get_steps_by_degrees(degrees_);
    this->move_steps(steps);
}

void StepperMotor::move_steps(long steps)
{
    this->stepper->step(steps);
}

int StepperMotor::get_steps_by_degrees(long degrees_)
{
    return (int)((long)(degrees_ * this->steps) / 360);
}